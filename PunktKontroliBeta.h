#ifndef PUNKTKONTROLIBETA_H
#define PUNKTKONTROLIBETA_H

#include "PunktKontroli.h"

class PunktKontroliBeta : public PunktKontroli {
public:
    PunktKontroliBeta(const string &nazwa, const int ilosc) : PunktKontroli(nazwa, ilosc) {
        PunktKontroli::iloscPK--;
    }

    ~PunktKontroliBeta() {
        PunktKontroli::iloscPK++;
    }

    double predykcja(PunktKontroli **pk, int k, int iloscPK) {
        double diff[iloscPK];
        for (int i = 0; i < iloscPK; i++) {
            diff[i] = 0;
            for (int j = 0; j < pk[i]->zwrocIloscPomiarowDoWykonania(); j++) {
                diff[i] += pow(pk[i]->pobierzPomiary()[j]->pobierzWartosc() - pomiary[j]->pobierzWartosc(), 2);
            }
            diff[i] = sqrt(diff[i]);
        }
        double collected[k];
        for (int i = 0; i < k; i++) {
            collected[i] = diff[i];
        }

        for (int i = k; i < iloscPK; i++) {
            double min = diff[i];
            for (int j = 0; j < k; j++) {
                if (min < collected[j]) {
                    swap(min, collected[j]);
                }
            }
        }
        double sum = 0;
        for (int i = 0; i < k; i++) {
            sum += collected[i];
        }
        return floor(sum / k);
    }
};

#endif
