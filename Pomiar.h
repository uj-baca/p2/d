//
// Created by Andret on 23.12.2018.
//

#ifndef POMIAR_H
#define POMIAR_H

#include <iostream>

using namespace std;

class Pomiar {
public:
    class Czas {
    public:
        Czas() : rok(0), miesiac(0), dzien(0), godzina(0), minuta(0), sekunda(0) {}

        Czas(int rok, int miesiac, int dzien, int godzina, int minuta, int sekunda) : rok(rok), miesiac(miesiac),
                                                                                      dzien(dzien), godzina(godzina),
                                                                                      minuta(minuta),
                                                                                      sekunda(sekunda) {}

        void wypisz() const {
            if (dzien < 10) {
                cout << 0;
            }
            cout << dzien << "-";
            if (miesiac < 10) {
                cout << 0;
            }
            cout << miesiac << "-";
            if (rok < 10) {
                cout << 0 << 0 << 0;
            } else if (rok < 100) {
                cout << 0 << 0;
            } else if (rok < 1000) {
                cout << 0;
            }
            cout << rok << " ";
            if (godzina < 10) {
                cout << 0;
            }
            cout << godzina << ":";
            if (minuta < 10) {
                cout << 0;
            }
            cout << minuta << ":";
            if (sekunda < 10) {
                cout << 0;
            }
            cout << sekunda << endl;
        }

    private:
        int rok, miesiac, dzien, godzina, minuta, sekunda;
    };

private:
    const double w;
    const Czas c;
public:
    Pomiar(double w, const Czas &c) : w(w), c(c) {}

    void informacja() const {
        c.wypisz();
        cout << w << endl;
    }

    const Czas &dataPomiaru() const {
        return c;
    }

    double pobierzWartosc() const {
        return w;
    }
};

#endif
