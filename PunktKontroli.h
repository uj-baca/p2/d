//
// Created by Andret on 23.12.2018.
//

#ifndef PUNKTKONTROLI_H
#define PUNKTKONTROLI_H

#include <cmath>
#include "Pomiar.h"

class PunktKontroli {
public:
    PunktKontroli(const string &nazwa, const int ilosc) : nazwa(nazwa), ilosc(ilosc) {
        pomiary = new Pomiar *[ilosc];
        count = 0;
        iloscPK++;
    }

    ~PunktKontroli() {
        iloscPK--;
    }

    int zwrocIloscPomiarowDoWykonania() const {
        return ilosc;
    }

    Pomiar *const *pobierzPomiary() {
        return pomiary;
    }

    static int iloscPunktowKontroli() {
        return iloscPK;
    }

    void
    zarejestrujPomiar(double wartoscZmierzona, int rok, int miesiac, int dzien, int godzina, int minuta, int sekunda) {
        if (count < ilosc) {
            pomiary[count++] = new Pomiar(wartoscZmierzona,
                                          Pomiar::Czas(rok, miesiac, dzien, godzina, minuta, sekunda));
        }
    }

    void komunikat() const {
        cout << nazwa << ":" << endl;
        for (int i = 0; i < count; i++) {
            pomiary[i]->informacja();
        }
    }

    virtual double predykcja(PunktKontroli **pk, int k, int iloscPK) {
        double diff[iloscPK];
        for (int i = 0; i < iloscPK; i++) {
            diff[i] = 0;
            for (int j = 0; j < count; j++) {
                diff[i] += abs(pk[i]->pomiary[j]->pobierzWartosc() - this->pomiary[j]->pobierzWartosc());
            }
        }
        double collected[k];
        for (int i = 0; i < k; i++) {
            collected[i] = diff[i];
        }
        for (int i = k; i < iloscPK; i++) {
            double min = diff[i];
            for (int j = 0; j < k; j++) {
                if (min < collected[j]) {
                    swap(min, collected[j]);
                }
            }
        }
        double sum = 0;
        for (int i = 0; i < k; i++) {
            sum += collected[i];
        }
        return floor(sum / k);
    }

protected:
    static int iloscPK;
    Pomiar **pomiary;
    int count;
private:
    const string nazwa;
    const int ilosc;
};

int PunktKontroli::iloscPK = 0;

#endif
